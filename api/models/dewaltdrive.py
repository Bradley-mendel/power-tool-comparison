from pydantic import BaseModel

class Error(BaseModel):
    message: str

class DriveIn(BaseModel):
    name: str
    torque: int
    rpm: int
    weight: int
    chuck: int
    chuck_type: str

class DriveOut(BaseModel):
    id: str
    name: str
    torque: int
    rpm: int
    weight: int
    chuck: int
    chuck_type: str
