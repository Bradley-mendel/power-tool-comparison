from queries.client import MongoQueries
from bson import ObjectId
from typing import Optional, Union, List
from models.milwaukeedrive import DriveIn, DriveOut, Error


class DuplicateAccountError(ValueError):
    pass


class ItemRepository(MongoQueries):


    def get_drive(self, item_id: str) -> Optional[DriveOut]:
        drive_queries = MongoQueries(collection_name="drive")
        record = drive_queries.collection.find_one(
            {"_id": ObjectId(item_id)})
        if record:
            return self.record_to_item_out(record)
        else:
            return {"message": f"Could not find that {item_id}"}


    def delete_drive(self, item_id: str) -> bool:
        drive_queries = MongoQueries(collection_name="drive")
        result = drive_queries.collection.delete_one(
            {"_id": ObjectId(item_id)})
        return result.deleted_count > 0


    def add_drive(self, item: DriveIn) -> Union[DriveOut, Error]:
        drive_queries = MongoQueries(collection_name="drive")
        try:
            item_dict = item.dict()
            result = drive_queries.collection.insert_one(item_dict)
            item_dict["id"] = str(result.inserted_id)
            del item_dict["_id"]
            return DriveOut(**item_dict)
        except Exception as e:
            return Error(detail=str(e))


    def item_in_to_out(self, id: int, item: DriveIn) -> DriveOut:
        return DriveOut(id=id, **item.dict())


    def update_drive(self, item_id: int, item: DriveIn) -> Union[DriveOut, Error]:
        drive_queries = MongoQueries(collection_name="drive")
        item_dict = item.dict()
        result = drive_queries.collection.update_one(
            {"_id": ObjectId(item_id)},
            {"$set": item_dict}
        )
        if result.matched_count:
            return self.item_in_to_out(item_id, item)
        else:
            return {"message": f"Could not update {item.name}"}


    def record_to_item_out(self, record) -> DriveOut:
        if '_id' in record:
            record['id'] = str(record['_id'])
            del record['_id']
        required_fields = ['id', 'name']
        for field in required_fields:
            if field not in record:
                print(f'Missing field: {field}')
        return DriveOut(**record)
